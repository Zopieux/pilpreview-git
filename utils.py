# -*- coding: utf-8 -*-

# Copyright 2008-2012 Alexandre `Zopieux` Macabies
# This module is part of avatarsio and is released under
# the MIT License: http://www.opensource.org/licenses/mit-license.php

from threading import RLock

class RedirectedStream(object):
    def __init__(self, original):
        self.original = original
        self.redirect = original
        self.lock = RLock()

    def redirect_start(self, redirect):
        self.lock.acquire()
        self.redirect = redirect

    def redirect_end(self):
        self.redirect = self.original
        self.lock.release()

    def __getattr__(self, name):
        self.lock.acquire()
        ret = getattr(self.redirect, name)
        self.lock.release()
        return ret
